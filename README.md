# Linux Tweet App

This is very simple NGINX website that allows a user to send a tweet. 

It's mostly used as a sample application for Docker. 

In this project, we use the built-in continuous integration in GitLab.

To use it:

- For every commit on the `main` branch (including merge), Build and Publish the resulting image on the Google Artifact Registry using the commit hash as the image tag.
- For every opened Pull Request (or merge request on GitLab) targeting the `main` branch, we build the image but do not publish it anywhere.

If the image have been published on the Artifact Registry successfully, the image will be deployed in Google Cloud Run automatically to access the application via a public URL.
Project URL: [Linux Tweet App](https://linux-tweet-app-xd64a5b35a-uc.a.run.app/) 
